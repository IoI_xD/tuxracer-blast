# Tux Racer Blast

A fork of Tux Racer Extreme that aims to improve the game to my, and hopefully others, liking.

Progess/plans:

- [ ] Revamp the graphics and aim for a PS1 aesthetic (modern graphics are not planned to keep compatibility with Windows 98).
- [ ] Remove the other characters, perhaps add some of Linux's scrapped mascots instead.
- [ ] Add controller support
- [ ] An online leaderboard, and a general revamped one then lets you sort by time.
- [ ] New music tracks and sound effects.
